import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import moment from 'moment';
import './App.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			albums: [],
			total: 0,
			query: "",
			currentPage: 0,
			isLoadingMore: false,
			isSearching: false
		}
	}

	componentDidMount = () => {
		document.title = "Spotify Albums Search";
	}

	handleKeyPress = (e) => {
		if (e.key === 'Enter') {
			this.search(e);
		}
	}

	search = (e) => {
		e.preventDefault();
		this.setState({ albums: [], total: 0, currentPage: 0, isSearching: true });
		const { query } = this.state;
		if (query.length > 0) {
			axios.get('/albums', { params: { query } })
				.then(({ data }) => {
					if (data.nInserted > 0) toast.success(`${data.nInserted} new album${data.nInserted > 1 && 's'} inserted to local catalogue`);
					this.setState({ albums: data.items, total: data.total, isSearching: false });
				})
				.catch(error => {
					this.setState({ isSearching: false });
					toast.error("There was an error making the query, try again later");
				});
		} else {
			this.setState({ isSearching: false });
		}
	}

	showMore = () => {
		this.setState({ isLoadingMore: true });
		const { query, currentPage } = this.state;
		axios.get('/albums', { params: { query, limit: 20, page: currentPage + 1 } })
			.then(({ data }) => {
				if (data.nInserted > 0) toast.success(`${data.nInserted} new album${data.nInserted > 1 && 's'} inserted to local catalogue`);
				this.setState({ albums: [...this.state.albums, ...data.items], currentPage: currentPage + 1, isLoadingMore: false });
			})
			.catch(error => {
				this.setState({ isSearching: false });
				toast.error("There was an error making the query, try again later");
			});
	}

	render() {
		const { albums, total, currentPage, query, isLoadingMore, isSearching } = this.state;
		return (
			<div className="App">
				<section className="section">
					<nav className="navbar NavSearch is-fixed-top">
						<div className="colummns">
							<div className="column">
								<h1 className="title">Spotify Albums Search</h1>
							</div>
							<div className="column">
								<div className="field has-addons search-box">
									<div className="control is-expanded">
										<input
											type="text"
											className="input is-rounded is-medium"
											id="search"
											placeholder="Search album on Spotify ...."
											value={query}
											onChange={e => { this.setState({ query: e.target.value }) }}
											onKeyPress={this.handleKeyPress}
										/>
									</div>
									<div className="control">
										<button className={"button is-rounded is-success is-medium " + (isSearching ? 'is-loading' : '')} onClick={this.search}>Search</button>
									</div>
								</div>
							</div>
							<div className="column">
								{total > 0 && <p>Total: {total}</p>}
							</div>
						</div>
					</nav>
				</section>

				<section className="section cards">
					<div className="container content-wrapper">
						<div className="columns is-multiline">
							{
								albums.map((album, index) => {
									return <div className="column is-3 desktop-only" key={index}>
										<div className="card">
											<header className="card-header">
												<p className="card-header-title">
													<span>{album.name}</span>
												</p>
											</header>
											<div className="card-content">
												<div className="content">
													<img src={album.images[0].url} alt="Admin template screenshot" />
												</div>

												<div className="media">
													<div className="media-content">
														<p><b>{artistNames(album.artists)}</b></p>
														<p><b className="total-tracks">Total Tracks:</b> {album.total_tracks}</p>
														<p>{moment(album.release_date, 'YYYY-mm-dd').format('YYYY')}</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								})
							}
						</div>
					</div>
				</section>
				{
					total > 0 && (total - ((currentPage + 1) * 20)) > 0 &&
					<button
						className={"button showButton is-rounded is-success is-medium " + (isLoadingMore ? 'is-loading' : '')}
						onClick={this.showMore}>Show more
					</button>
				}
				<ToastContainer />
			</div>
		);
	}
}

const artistNames = (artists) => {
	const names = artists.map(artist => artist.name);
	return names.join(", ");
}

export default App;
