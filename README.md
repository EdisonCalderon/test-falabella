# Technical Test for Falabella
Application built to consult albums in the Spotify API

The application is responsible for generating Token to communicate with the Spotify API, and automatically renew it once it has expired.

The application stores only the new albums obtained when making a query, and throws a notification notifying how many new albums have been stored

* It is built with the Stack MERN
* Mongo Atlas is used (Database as a service)
* The access credentials are in .env file

## How to run it?

To install dependencies, run below command in root folder, it will install root, client and server dependencies

```console
user@falabella:~$ npm install
```

If you want to run with node

```console
user@falabella:~$ npm run start
```

If you want to run with nodemon

```console
user@falabella:~$ npm run dev
```

## Functionalities


The functionalities of the application are shown below

### Show more
![SHOW MORE](docs/show_more.gif)

### Notifications for new albums
![NOTIFICATIONS](docs/notifications.gif)

## Responsive design
![RESPONSIVE](docs/responsive.gif)