import axios from 'axios';
import { SpotifyAuth } from "./SpotifyAuth";
const auth = new SpotifyAuth(process.env.SPOTIFY_CLIENT_ID, process.env.SPOTIFY_CLIENT_SECRET);

const search = async (options) => {
    try {
        const token = await auth.getToken();
        const { data } = await axios('https://api.spotify.com/v1/search',
            {
                method: 'GET',
                params: {
                    q: options.q,
                    type: options.type,
                    market: options.market,
                    limit: options.limit,
                    offset: options.offset
                },
                headers: {
                    Authorization: token
                }
            });
            const {items, total} = data.albums;
        return { items, total};
    } catch (error) {
        throw error;
    }    
}

export default { search };