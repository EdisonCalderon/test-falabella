import { Router } from 'express';
import SpotifyAPI from "../spotify/SpotifyAPI";
import AlbumsController from "./albums.controller";
const router = Router();

router.get('/', async (req, res) => {
    const q = req.query.query;
    const page = (req.query.page) ? req.query.page : 0;
    const limit = 20;
    const offset = limit * page;
    try {
        const response = await SpotifyAPI.search({ q, limit, offset, type: "album" });
        const nInserted = await AlbumsController.saveAlbums(response.items);
        response.nInserted = nInserted;
        return res.status(200).json(response);
    } catch (error) {
        return res.json(500).json({error: error.message});
    }
});

export default router;
