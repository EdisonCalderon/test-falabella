import AlbumsModel from "./albums.model";

const saveAlbums = async (albums) => {
    try {
        await AlbumsModel.collection.insertMany(albums, { ordered: false });
        return albums.length;
    } catch (error) {
        if (error.code === 11000) {
            return error.result.result.nInserted;
        } else {
            throw error;
        }
    }
}

export default { saveAlbums };